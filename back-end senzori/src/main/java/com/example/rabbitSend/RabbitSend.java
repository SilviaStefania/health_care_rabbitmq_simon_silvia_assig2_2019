package com.example.rabbitSend;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
public class RabbitSend implements CommandLineRunner {

    private final static String QUEUE_NAME = "hello";

    @Override
    public void run(String... args) throws Exception {

        File file = new File("D:\\desktop\\CTI faculta\\AN 4 SEM 1\\ds\\assig1\\rabbitSend\\rabbitSend\\senzori.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        Random rand = new Random();
        String message;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            while ((message = br.readLine()) != null){
               TimeUnit.SECONDS.sleep(1);

                String[] theParsing = message.split("\t\t", 3);

                LocalDateTime startDate = LocalDateTime.parse(theParsing[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                LocalDateTime endDate = LocalDateTime.parse(theParsing[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                long startDateMillis = startDate.toEpochSecond(ZoneOffset.UTC);
                long endDateMillis = endDate.toEpochSecond(ZoneOffset.UTC);
                long ceva = endDateMillis - startDateMillis;
                int verif = 0;
                if(ceva > 43200000)
                   verif = 1;
                String[] act = theParsing[2].split("\t");

                String finalMessage = "{" + "\n" +
                        "\"patient_id\":" + " \"3\" " + "\n" +
                "\"activity\":" + "\"" + act[0]+"\"" +"\n"+
                        "\"start\":" + startDateMillis+"\n"+
                        "\"end\":"  + endDateMillis+"\n"+
                        "}";

                channel.basicPublish("", QUEUE_NAME, null, finalMessage.getBytes("UTF-8"));
               System.out.println( finalMessage );
            }

        }
    }

}
