package com.example.springdemo.controller;

import com.example.springdemo.dto.CaregiverSeenByDoctor;
import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.Roles;
import com.example.springdemo.entities.SessionId;
import com.example.springdemo.services.AppUserService;
import com.example.springdemo.webSocket.BaseEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequiredArgsConstructor
public class CaregiverController {
    private final AppUserService appUserService; //pentru caregiver
    private final SimpMessagingTemplate messagingTemplate;

    @GetMapping(value = "/caregivers")
    public List<CaregiverSeenByDoctor> takeCaregivers() {
        List<AppUser> allCaregivers = appUserService.findAllCaregivers();
        List<CaregiverSeenByDoctor> caregiverSeenByDoctors = new ArrayList<>();
        for (AppUser caregiver : allCaregivers) {
            caregiverSeenByDoctors.add(new CaregiverSeenByDoctor(caregiver));
        }
        return caregiverSeenByDoctors;
    }

    @PostMapping(value = "/createCaregiver")
    public void addCaregiver(@RequestBody AppUser appUser) {
        appUser.setRole(Roles.CAREGIVER);
        appUserService.save(appUser);
    }

    @DeleteMapping(value = "/deleteCaregiver")
    public String deleteCaregiver(@RequestBody SessionId receivedId) {
        int id = Integer.parseInt(receivedId.getIdToDelete());
        String confirmationMessage = "Id is not present.";
        if (appUserService.findById(id).isPresent()) {
            if (appUserService.findAllCaregivers().contains(appUserService.findById(id).get())) {
                appUserService.removeAppUser(appUserService.findById(id).get());
                confirmationMessage = "Caregiver removed. Refresh to see.";
            }
        }
        return confirmationMessage;
    }

    @PutMapping(value = "/updateCaregiver")
    public String updateCaregiver(@RequestBody SessionId session) {
        String confirmation = "There is no caregiver to update";
        Integer id = Integer.parseInt(session.getIdToUpdate());
        if (appUserService.findById(id).isPresent()) {
            if (appUserService.findAllCaregivers().contains(appUserService.findById(id).get())) {
                confirmation = "Update done.";
                AppUser CaregiverToUpdate = appUserService.findById(id).get();
                if (!session.getName().equals("")) {
                    CaregiverToUpdate.setName(session.getName());
                }
                if (!session.getGender().equals("")) {
                    CaregiverToUpdate.setGender(session.getGender());
                }
                if (!session.getAddress().equals("")) {
                    CaregiverToUpdate.setAddress(session.getAddress());
                }
                if (!session.getBday().equals("")) {
                    CaregiverToUpdate.setBday(session.getBday());
                }
                appUserService.save(CaregiverToUpdate);
            }
        }
        return confirmation;

    }

    @EventListener(BaseEvent.class)
    public void handleEvent(BaseEvent event) {
        log.info("Got an event: {}.", event);
        messagingTemplate.convertAndSend("/topic/events", event);
    }


}
