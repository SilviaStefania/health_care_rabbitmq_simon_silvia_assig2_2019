package com.example.springdemo.controller;

import com.example.springdemo.dto.MedicalPlansSeenByPatient;
import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.SessionId;
import com.example.springdemo.services.AppUserService;
import com.example.springdemo.services.MedicalPlanService;
import com.example.springdemo.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin
public class MedicalPlanController {

    private final AppUserService appUserService;
    private final MedicationService medicationService;
    private final MedicalPlanService medicalPlanService;


    @PostMapping(value = "/medicalPlans")
    public  List<MedicalPlansSeenByPatient> getMedPlansForPatient(@RequestBody SessionId sessionId){
        AppUser thePatient = appUserService.findById(Integer.parseInt(sessionId.getSessionId())).get();
        List<MedicalPlan> medicalPlans = medicalPlanService.findAllMedicalPlansForPatient(thePatient);
        List<MedicalPlansSeenByPatient>  medicalPlansSeenByPatients = new ArrayList<>();

        for(MedicalPlan medicalPlan: medicalPlans){
            medicalPlansSeenByPatients.add(new MedicalPlansSeenByPatient(medicalPlan));
        }

        return medicalPlansSeenByPatients;

    }

    @PostMapping(value = "/addMedicalPlan")
    public String addMedPlanToPatient(@RequestBody SessionId myData){
        AppUser patient = appUserService.findById(Integer.parseInt(myData.getPatientId())).get();
        AppUser doctor = appUserService.findById(Integer.parseInt(myData.getDoctorId())).get();
        String startMed = myData.getStartMed();
        String stopMed = myData.getStopMed();
        String timePeriod = myData.getTimePeriod();

        List<Integer> medsId = myData.getMeds();
        List<Medication> meds = new ArrayList<>();
        for(Integer medicationIdToAdd: medsId){
            meds.add(medicationService.findById(medicationIdToAdd).get());
        }

        MedicalPlan medicalPlan = new MedicalPlan(doctor, patient, startMed, stopMed, timePeriod, meds);

        medicalPlanService.save(medicalPlan);

        return "Medical Plan added";
    }

}
