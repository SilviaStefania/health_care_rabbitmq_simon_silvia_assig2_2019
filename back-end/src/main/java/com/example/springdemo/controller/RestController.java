package com.example.springdemo.controller;

import com.example.springdemo.dto.PatientSeenByDoctorDTO;
import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.Roles;
import com.example.springdemo.entities.SessionId;
import com.example.springdemo.services.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin
@RequiredArgsConstructor
public class RestController {
    private final AppUserService appUserService;  //pentru patient

    @PostMapping(value = "/patients")
    public List<PatientSeenByDoctorDTO> getPatients(@RequestBody SessionId sessionId) {
        Integer id = Integer.parseInt(sessionId.getSessionId());
        Optional<AppUser> appUser = appUserService.findById(Integer.parseInt(sessionId.getSessionId()));

        List<PatientSeenByDoctorDTO> patients = new ArrayList<>();

        if (appUser.isPresent()) {
            List<AppUser> patientsByRole;

            if (appUser.get().getRole().equals(Roles.DOCTOR)) {
                patientsByRole = appUserService.findAllPatients();
            } else {
                patientsByRole = appUserService.findAllPatientsOfCaregiver(appUser.get());

            }

            for (AppUser patient : patientsByRole) {
                patients.add(new PatientSeenByDoctorDTO(patient));
            }

        }
        return patients;
    }


    @PostMapping(value = "/createPatient")
    public void createNewPatient(@RequestBody AppUser appUser) {
        appUser.setRole(Roles.PATIENT);
        appUserService.save(appUser);
    }

    @DeleteMapping(value = "/deletePatient")
    public String deletePatient(@RequestBody SessionId receivedId) {
        int id = Integer.parseInt(receivedId.getIdToDelete());
        String confirmationMessage = "Id is not present.";
        if (appUserService.findById(id).isPresent()) {
            if (appUserService.findAllPatients().contains(appUserService.findById(id).get())) {
                appUserService.removeAppUser(appUserService.findById(id).get());
                confirmationMessage = "Patient removed. Refresh to see.";
            }
        }
        return confirmationMessage;
    }


    @PostMapping(value = "/updatePatient")
    public String updatePatient(@RequestBody SessionId session ){
        String confirmation = "There is no patient to update";
        Integer id = Integer.parseInt(session.getIdToUpdate());
        if(appUserService.findById(id).isPresent()){
            if(appUserService.findAllPatients().contains(appUserService.findById(id).get())){
                confirmation = "Update done.";
                AppUser patientToUpdate = appUserService.findById(id).get();
               if(!session.getName().equals("")){
                   patientToUpdate.setName(session.getName());
               }
               if(!session.getGender().equals("")){
                   patientToUpdate.setGender(session.getGender());
               }
               if(!session.getAddress().equals("")){
                   patientToUpdate.setAddress(session.getAddress());
               }
              if(!session.getBday().equals("")){
                  patientToUpdate.setBday(session.getBday());
              }
              appUserService.save(patientToUpdate);
            }
        }
        return confirmation;

    }


}
