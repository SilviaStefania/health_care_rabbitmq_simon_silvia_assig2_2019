package com.example.springdemo.dto;

import com.example.springdemo.entities.AppUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaregiverSeenByDoctor {

    private Integer id;
    private String name;
    private String address;
    private String gender;
    private String bday;

    public CaregiverSeenByDoctor(AppUser appUser){
        this.id = appUser.getAppUserId();
        this.name = appUser.getName();
        this.address = appUser.getAddress();
        this.gender = appUser.getGender();
        this.bday = appUser.getBday();
    }



}
