package com.example.springdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NotificationDto {

    private String patientName;
    private String problem;

    public String toString(){
        String noti = "";
        if(problem.equals("Sleeping")){
            noti = "Patient " + patientName + " has slept more than 12 hours";
        }
        else if (problem.equals("Leaving")){
            noti = "Patient " + patientName + " has left the house for more than 12 hours";
        }
        else if (problem.equals("Toileting")){
            noti = "Patient " + patientName + ":  The period spent in bathroom is longer than 1 hour;";
        }
        return noti;
    }


}
