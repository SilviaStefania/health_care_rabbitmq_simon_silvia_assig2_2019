package com.example.springdemo.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "patientActivity")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "patient_activity")
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer actId;
    String activity;
    String startTime;
    String endTime;

    @ManyToOne
    @JoinColumn(name = "patientId", nullable = false)
    private AppUser patient;

    public Activity(String activity, String startTime, String endTime, AppUser patient){
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
        this.patient = patient;
    }

    public String toString(){
        return this.activity +" " + this.startTime + " " + this.endTime + " " + this.patient + "\n";
    }

}
