package com.example.springdemo.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "medical_plan")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medical_plan")

public class MedicalPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medicalPlanId;

    @ManyToOne
    @JoinColumn(name = "patientId", nullable = false)
    private AppUser patient;

    @ManyToOne
    @JoinColumn(name = "doctorId", nullable = false)
    private AppUser doctor;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "medical_plan_has_med",
            joinColumns = {@JoinColumn(name = "medicalPlanId")},
            inverseJoinColumns = {@JoinColumn(name = "medId")}
    )
    private List<Medication> medications = new ArrayList<>();

    private String startMed;
    private String stopMed;
    private String timePeriod;

    public MedicalPlan(String startMed, String stopMed, String timePeriod, List<Medication> medications) {
        this.startMed = startMed;
        this.stopMed = stopMed;
        this.timePeriod = timePeriod;
        this.medications = medications;
      //  this.medications.forEach(m -> m.getMedicalPlans().add(this));
    }


    public MedicalPlan(AppUser doctor, AppUser patient, String startMed, String stopMed, String timePeriod, List<Medication> medications) {
        this.patient = patient;
        this.doctor = doctor;
        this.startMed = startMed;
        this.stopMed = stopMed;
        this.timePeriod = timePeriod;
        this.medications = medications;
    //    this.medications.forEach(m -> m.getMedicalPlans().add(this));
    }

}
