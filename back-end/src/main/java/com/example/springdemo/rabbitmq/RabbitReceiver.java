package com.example.springdemo.rabbitmq;

import com.example.springdemo.dto.NotificationDto;
import com.example.springdemo.entities.Activity;
import com.example.springdemo.entities.AppUser;
import com.example.springdemo.repositories.RepositoryFactory;
import com.example.springdemo.webSocket.BaseEvent;
import com.example.springdemo.webSocket.CaregiverReceivesNotification;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RabbitReceiver implements CommandLineRunner {

    private final static String QUEUE_NAME = "hello";
    private final RepositoryFactory repositoryFactory;

    private final ApplicationEventPublisher eventPublisher;

    @Override
    public void run(String... args) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");

            String[] senzorInputs = message.split("}");
            for(String senzorInput: senzorInputs){
                String[] divideInput = senzorInput.split("[\n:{}\"\"]+");
                String startTime = divideInput[8];
                String endTime = divideInput[10];
                String act = divideInput[6];
                AppUser appUser = repositoryFactory.createAllUsersRepository().findById(Integer.parseInt(divideInput[3])).get();
                Activity test = new Activity(act, startTime, endTime,
                       appUser);
                repositoryFactory.createActivityRepository().save(test);

                String caregiverNotification = "";

                //aplicare reguli
                if(act.equals("Sleeping") && Integer.parseInt(endTime)-Integer.parseInt(startTime) >= 43200){
                    NotificationDto notificationDto = new NotificationDto(appUser.getName(), act);
                    caregiverNotification = notificationDto.toString();
                    eventPublisher.publishEvent(new CaregiverReceivesNotification(notificationDto));
                }
                if(act.equals("Leaving") && Integer.parseInt(endTime)-Integer.parseInt(startTime) >= 43200){
                    NotificationDto notificationDto = new NotificationDto(appUser.getName(), act);
                    caregiverNotification = notificationDto.toString();
                    eventPublisher.publishEvent(new CaregiverReceivesNotification(notificationDto));
                }
                if(act.equals("Toileting") && Integer.parseInt(endTime)-Integer.parseInt(startTime) >= 3600){
                    NotificationDto notificationDto = new NotificationDto(appUser.getName(), act);
                    caregiverNotification = notificationDto.toString();
                    eventPublisher.publishEvent(new CaregiverReceivesNotification(notificationDto));
                }
                System.out.println(caregiverNotification);
              //  eventPublisher.publishEvent(caregiverNotification);


            }
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }





}
