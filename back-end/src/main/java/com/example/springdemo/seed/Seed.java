/*package com.example.springdemo.seed;

import com.example.springdemo.entities.AppUser;
import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Roles;
import com.example.springdemo.repositories.RepositoryFactory;
import com.example.springdemo.services.AppUserService;
import com.example.springdemo.services.MedicalPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class Seed implements CommandLineRunner {

    private final RepositoryFactory repositoryFactory;
    private final AppUserService appUserService;
    private final MedicalPlanService medicalPlanService;

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        List<AppUser> patientsOfCaregiver1 = new ArrayList<>();
        List<AppUser> patientsOfCaregiver2 = new ArrayList<>();

        AppUser doctor1 = new AppUser(null, "Doctor1",
                "d1", Roles.DOCTOR, "doctor1", "22/02/1997", "M", "Cluj");
        appUserService.save(doctor1);

        AppUser doctor2 = new AppUser(null, "Doctor2",
                "d2", Roles.DOCTOR, "doctor2", "22/02/1997", "M", "Cluj");
        appUserService.save(doctor2);

        AppUser patient1 = new AppUser(null, "Pacient1",
                "p1", Roles.PATIENT, "pacient1", "22/02/1997", "M", "Cluj");
        patientsOfCaregiver1.add(repositoryFactory.createAllUsersRepository().save(patient1));

        AppUser patient2 = new AppUser(
                null, "Pacient2",
                "p2", Roles.PATIENT, "pacient2", "22/02/1997", "F", "Cluj");
        patientsOfCaregiver1.add(repositoryFactory.createAllUsersRepository().save(patient2));

        patientsOfCaregiver2.add(repositoryFactory.createAllUsersRepository().save(new AppUser(null, "Pacient3",
                "p3", Roles.PATIENT, "pacient3", "22/02/1997", "F", "Cluj")));
        patientsOfCaregiver2.add(repositoryFactory.createAllUsersRepository().save(patient1));


        AppUser c1 = repositoryFactory.createAllUsersRepository().save(new AppUser(null, "C2",
                "c2", Roles.CAREGIVER, "c2", "22/02/1997", "F", "Cluj", patientsOfCaregiver1));

        AppUser c2 = repositoryFactory.createAllUsersRepository().save(new AppUser(null, "C3",
                "c3", Roles.CAREGIVER, "c3", "22/02/1997", "F", "Cluj", patientsOfCaregiver2));


        appUserService.findAllPatientsOfCaregiver(c1).forEach(System.out::println);

        Medication medication1 = repositoryFactory.createMedicationRepository().save(new Medication(null, "Paracetamol",
                "3 per day", "you turn green"));
        Medication medication2 = repositoryFactory.createMedicationRepository().save(new Medication(null, "Nurofen",
                "4 per day", "you walk on walls"));
        Medication medication3 = repositoryFactory.createMedicationRepository().save(new Medication(null, "Antibiotic",
                "5 per day", "you cant drink"));
        Medication medication4 = repositoryFactory.createMedicationRepository().save(new Medication(null, "ParaSinus",
                "3 per day", "you dont stop sneezing"));


        List<Medication> medsForPlan1 = new ArrayList<>();
        medsForPlan1.add(medication1);
        medsForPlan1.add(medication2);
        medicalPlanService.save(new MedicalPlan(doctor1, patient1,
                "03/11/2019", "09/11/2019", "3 days", medsForPlan1));

        List<Medication> medsForPlan2 = new ArrayList<>();
        medsForPlan2.add(medication1);
        medsForPlan2.add(medication3);
        medsForPlan2.add(medication4);
        medicalPlanService.save(new MedicalPlan(doctor2, patient2,
                "03/11/2019", "09/11/2019", "3 days", medsForPlan2));


        medicalPlanService.findAllMedicalPlansForPatient(patient1).forEach(e -> System.out.println(e.getMedications()));


    }


}
*/