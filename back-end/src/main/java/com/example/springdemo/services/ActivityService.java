package com.example.springdemo.services;

import com.example.springdemo.entities.Activity;
import com.example.springdemo.repositories.RepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ActivityService {

    private final RepositoryFactory repositoryFactory;


    public Activity save(Activity activity) {
        return repositoryFactory.createActivityRepository().save(activity);
    }

  public void removeActivity(Activity activity) {
        repositoryFactory.createActivityRepository().delete(activity);
    }

    public Optional<Activity> findById(int id) {
        return repositoryFactory.createActivityRepository().findById(id);
    }



}
