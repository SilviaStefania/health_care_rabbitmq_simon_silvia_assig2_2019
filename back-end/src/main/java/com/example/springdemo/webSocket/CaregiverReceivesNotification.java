package com.example.springdemo.webSocket;

import com.example.springdemo.dto.NotificationDto;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class CaregiverReceivesNotification extends BaseEvent{

    private final NotificationDto notificationDto;

public CaregiverReceivesNotification(NotificationDto notificationDto){
    super(EventType.CAREGIVER_RECEIVE_NOTIFICATION);
    this.notificationDto = notificationDto;
}


}
