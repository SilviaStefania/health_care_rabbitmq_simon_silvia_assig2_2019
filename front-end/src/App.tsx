import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from "./components/Login"
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import CssBaseline from "@material-ui/core/CssBaseline";
import DoctorMainPage from "./components/DoctorMainPage";
import CaregiverMainPage from "./components/CaregiverMainPage";
import PatientMainPage from "./components/PatientMainPage"

const App: React.FC = () => {
    return (
        <BrowserRouter>
            <>
                <CssBaseline>
                    <Switch>
                        <Route exact path="/" component={Login}/>
                        {
                            localStorage.getItem("role") == "DOCTOR" &&   <Route exact path="/doctor" component={DoctorMainPage}/>
                        }

                        {
                            localStorage.getItem("role") == "CAREGIVER" &&   <Route exact path="/caregiver" component={CaregiverMainPage}/>
                        }

                        {
                            localStorage.getItem("role") == "PATIENT" &&   <Route exact path="/patient" component={PatientMainPage}/>
                        }

                    </Switch>

                </CssBaseline>

            </>
        </BrowserRouter>

    );
};

export default App;
