import {makeStyles, Paper, Table} from "@material-ui/core";
import React, {useState} from "react";
import SimpleTable from "./Table";
import PatientsTable from "./tables/PatientsTable";
import Button from "@material-ui/core/Button";
import WebSocketListener from "../webSocket/WebSocketListener";



const listener= new WebSocketListener();

const caregiverStyle = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),

    },
    h1: {
        marginLeft: theme.spacing(7),
    },
    h3: {
        marginLeft: theme.spacing(4),
    },
    button: {
        margin: "20px 20px",
    },

}));


export default function () {
    const classes = caregiverStyle();

    function logout(){
        window.location.assign("/");
        localStorage.clear();
    }
const [notification, setNotification] = useState("");
    listener.on("event", event =>
    {
        if(event.type === "CAREGIVER_RECEIVE_NOTIFICATION"){
            setNotification(notification + event.notificationDto.patientName + " " + event.notificationDto.problem + "\n")
        }
    });

    return (

        <div className={classes.paper}>

            <form>

                <h1 className={classes.h1}>

                    <Button className={classes.button} onClick={() => logout()}
                            type="button"
                            variant="contained"
                    >
                        Logout
                    </Button>

                    These are your patients. Take good care of them :)
                </h1>
            </form>

            <Paper>

                <PatientsTable/>

                {
                    notification
                }

            </Paper>

        </div>


    );
}