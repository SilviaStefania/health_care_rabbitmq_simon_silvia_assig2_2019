import React, {useState} from 'react';
import clsx from 'clsx';
import {makeStyles, Theme, createStyles} from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import Patient from "../../types/Patient";
import axios from "axios"


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dense: {
            marginTop: theme.spacing(2),
        },
        menu: {
            width: 200,
        },
        button: {
            margin: "20px 20px",
        },
    }),
);


export default function OutlinedTextFields() {
    const classes = useStyles();


    function updatePatient() {


        axios.post("http://localhost:8080/updatePatient", {idToUpdate:patientId, name, address, gender, bday}).then(response => {
                alert(response.data);
                window.location.reload();
            }
        );


        // @ts-ignore
        document.getElementById("outlined-name").value = "";
        // @ts-ignore
        document.getElementById("outlined-address").value = "";
        // @ts-ignore
        document.getElementById("outlined-gender").value = "";
        // @ts-ignore
        document.getElementById("outlined-bday").value = "";

    }

    const [patientId, setPatientId] = useState("");
    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const [gender, setGender] = useState("");
    const [bday, setBday] = useState("");

    return (
        <form className={classes.container} noValidate autoComplete="off">


            <TextField
                id="outlined-patientId"
                label="Patient Id"
                className={classes.textField}
                onChange={(e) => setPatientId(e.target.value)}
                margin="normal"
                variant="outlined"
            />


            <TextField
                id="outlined-name"
                label="Name"
                className={classes.textField}
                onChange={(e) => setName(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField
                id="outlined-address"
                label="Address"
                className={classes.textField}
                onChange={(e) => setAddress(e.target.value)}
                margin="normal"
                variant="outlined"
            />
            <TextField

                id="outlined-gender"
                label="Gender"
                onChange={(e) => setGender(e.target.value)}
                className={classes.textField}
                margin="normal"
                variant="outlined"
            />
            <TextField

                id="outlined-bday"
                label="Birth Day"
                onChange={(e) => setBday(e.target.value)}
                className={classes.textField}
                margin="normal"
                variant="outlined"
            />

            <Button className={classes.button} onClick={() => updatePatient()}
                    type="button"
                    variant="contained"
                    color="secondary"
            >
                Update Patient
            </Button>

        </form>
    );
}