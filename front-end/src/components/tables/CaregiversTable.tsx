import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Caregiver from "../../types/Caregiver";
import axios from "axios";

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    button: {
        margin: "20px 20px",
        backgroundColor: "indigo",
        color: "yellow",
    },
});

export default function CaregiverTable() {
    const classes = useStyles();
    const [rows, setRows] = useState<Caregiver[]>([]);

    useEffect(()=> {
        axios.get("http://localhost:8080/caregivers").then(response => {
            console.log(response.data);
            setRows(response.data);
        });
    },[]);

    return (
        <Paper className={classes.root}>
            <Table className={classes.table} stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell>Caregiver ID</TableCell>
                        <TableCell align="right">Name</TableCell>
                        <TableCell align="right">Address</TableCell>
                        <TableCell align="right">Gender</TableCell>
                        <TableCell align="right">Birthday</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.id}>
                            <TableCell  component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell align="right">{row.name}</TableCell>
                            <TableCell align="right">{row.address}</TableCell>
                            <TableCell align="right">{row.gender}</TableCell>
                            <TableCell align="right">{row.bday}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
}