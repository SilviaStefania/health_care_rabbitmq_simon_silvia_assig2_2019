import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Patient from "../../types/Patient";
import axios from "axios";
import {Button} from "@material-ui/core";
import CreateMedicalPlanByDoctor from "../forms/CreateMedicalPlanByDoctor"

const useStyles = makeStyles({
    root: {
        width: '100%',
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    button: {
        backgroundColor: "indigo",
        color: "yellow",
    },
});

export default function SimpleTable() {
    const classes = useStyles();
    const [rows, setRows] = useState<Patient[]>([]);

    useEffect(() => {
        axios.post("http://localhost:8080/patients", {sessionId: localStorage.getItem("sessionId")}).then(response => {
            debugger;
            console.log(response.data);
            setRows(response.data);
        });
    }, []);

    return (
        <Paper className={classes.root}>
            <Table className={classes.table} stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell>Patient ID</TableCell>
                        <TableCell align="right">Name</TableCell>
                        <TableCell align="right">Address</TableCell>
                        <TableCell align="right">Gender</TableCell>
                        <TableCell align="right">Birthday</TableCell>

                        {
                            localStorage.getItem("sessionId") == "1" && <TableCell align="right"> </TableCell>
                        }


                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell align="right">{row.name}</TableCell>
                            <TableCell align="right">{row.address}</TableCell>
                            <TableCell align="right">{row.gender}</TableCell>
                            <TableCell align="right">{row.bday}</TableCell>

                            {
                                localStorage.getItem("sessionId") == "1"
                                &&
                                <TableCell align="right">
                                    <CreateMedicalPlanByDoctor id = {row.id} address={row.address} bday={row.bday} gender={row.gender} name={row.name}/>
                                </TableCell>
                            }


                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
}