export default interface Caregiver {
    id?: number,
    name: string,
    address: string,
    gender: string,
    bday: string
}
;
