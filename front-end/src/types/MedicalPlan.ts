export default interface MedicalPlan{

    medicalPlanId? : number,
    patientId: number,
    doctorId: number,
    meds: number[],
    startMed: string,
    stopMed: string,
    timePeriod: string

};