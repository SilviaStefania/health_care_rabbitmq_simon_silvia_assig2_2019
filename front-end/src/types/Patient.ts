export default interface Patient {

    id?: number,
    name: string,
    address: string,
    gender: string,
    bday: string

}
;